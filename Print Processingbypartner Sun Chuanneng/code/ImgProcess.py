import cv2
import matplotlib.pyplot as plt
import numpy as np
import os


class ImgProcess:
    def __init__(self):
        pass

    @staticmethod
    def pdf2jpg(path):
        command = 'convert -density 144 %s[0] -resize 50%% %s'
        for f in os.listdir(path):
            # print(path+'/'+f)
            if (not os.path.isdir(f)) and ('.pdf' in f):
                os.system(command % (path+'/'+f, path+'/'+f.replace('.pdf', '.jpg')))
                # print(command % (path+'/'+f, path+'/'+f.replace('.pdf', '.jpg')))
            elif os.path.isdir(path+'/'+f):
                # print('yy')
                pdf2jpg(path+'/'+f)

    @staticmethod
    def showPic(img):
        plt.imshow(img)
        plt.show()

    @staticmethod
    def processPicOneHand(path, horizontal_length=14, vertical_length=14):
        image = cv2.imread(path)
        gray = cv2.cvtColor(image, cv2.COLOR_RGBA2GRAY)
        canny = cv2.Canny(gray, threshold1=50, threshold2=300)
        GauBlur = cv2.GaussianBlur(canny, (3, 3), 0)
        kernelOpen = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 20))
        close = cv2.morphologyEx(GauBlur, cv2.MORPH_CLOSE, kernelOpen)
        erode = cv2.erode(close, np.ones((1, horizontal_length)), iterations=4)
        erode = cv2.erode(erode, np.ones((vertical_length, 1)), iterations=4)
        dilate = cv2.dilate(erode, np.ones((25, 25)), iterations=4)
        img_copy = image.copy()
        _, cnts, hierarchy = cv2.findContours(dilate, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        sort_cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
        cnt = sort_cnts[0]
        rect = cv2.boundingRect(cnt)
        roi = img_copy[rect[1]: rect[1]+rect[3], rect[0]: rect[0]+rect[2]]
        cv2.imwrite(path.replace('.jpg', '')+'new.jpg', roi)

    @staticmethod
    def processPicTwoHands(path, horizontal_length=16, vertical_length=16):
        image = cv2.imread(path)
        gray = cv2.cvtColor(image, cv2.COLOR_RGBA2GRAY)
        canny = cv2.Canny(gray, threshold1=50, threshold2=300)
        GauBlur = cv2.GaussianBlur(canny, (3, 3), 0)
        kernelOpen = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 20))
        close = cv2.morphologyEx(GauBlur, cv2.MORPH_CLOSE, kernelOpen)
        erode = cv2.erode(close, np.ones((1, horizontal_length)), iterations=4)
        erode = cv2.erode(erode, np.ones((vertical_length, 1)), iterations=4)
        dilate = cv2.dilate(erode, np.ones((25, 25)), iterations=4)
        img_copy = image.copy()
        _, cnts, hierarchy = cv2.findContours(dilate, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        sort_cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
        cnt1, cnt2 = sort_cnts[0], sort_cnts[1]
        rect1 = cv2.boundingRect(cnt1)
        rect2 = cv2.boundingRect(cnt2)
        roi = img_copy[min(rect1[1], rect2[1]): max(rect1[1]+rect1[3], rect2[1]+rect2[3]),\
                       min(rect1[0], rect2[0]): max(rect1[0]+rect1[2], rect2[0]+rect2[2])]
        cv2.imwrite(path.replace('.jpg', '')+'new.jpg', roi)

    @staticmethod
    def processAllPics(path, num_files):
        areas = []
        print('Processing '+path)
        for f in os.listdir(path):
            try:
                if (not os.path.isdir(path+'/'+f)) and ('.jpg' in f):
                    if num_files % 2 == 0:
                        areas.append(ImgProcess.processPicTwoHands(path+'/'+f))
                    else:
                        areas.append(ImgProcess.PicOneHand(path+'/'+f))
                elif os.path.isdir(path+'/'+f):
                    ImgProcess.processAllPics(path+'/'+f, len(os.listdir(path+'/'+f)))
            except:
                with open('errorLog.txt', 'a', encoding='utf-8') as errorF:
                    errorF.write(path+'/'+str(f)+'\n')
                print('Error occurs in '+path+'/'+f)

    @staticmethod
    def getMeanSize(path):
        widths = 0.
        heights = 0.
        count = 0.
        pic = None
        for folder in os.listdir(path):
            for f in os.listdir(path+'/'+folder):
                if '.jpg' in f:
                    pic = cv2.imread(path+'/'+folder+'/'+f)
                    print('Processing '+path+'/'+folder+'/'+f+': '+str(pic.shape))
                    height, width, _ = pic.shape
                    widths += width
                    heights += height
                    count += 1.
        return (heights / count), (widths / count)

    @staticmethod
    def transform(img, height, width, color_list):
        '''
        @params:
        -img: The img that want to be filled
        -height, width: New img's height and width
        -color_list: The pixle list filled in the img, [0, 0, 255] this king of thing,
                    the type has to be type(list)
        @returns:
        -new_img: New picture.
        '''
        img_height, img_width, _ = img.shape
        new_img = img.copy()
        if height > img_height:
            up_pixles = int((height - img_height) // 2)
            down_pxles = int(height - img_height - up_pixles)
            new_img = np.row_stack((np.array([color_list] * up_pixles * img_width)\
                                    .reshape((up_pixles, img_width, 3)), new_img))
            new_img = np.row_stack((new_img, np.array([color_list] * down_pxles * img_width)\
                                            .reshape((down_pxles, img_width, 3))))
        else:
            up_pixles = int((img_height - height) // 2)
            down_pxles = int(img_height - height - up_pixles)
            new_img = new_img[up_pixles: -down_pxles, :, :]
        if width > img_width:
            left_pixles = int((width - img_width) // 2)
            right_pixles = int(width - img_width - left_pixles)
            new_img = np.column_stack((np.array([color_list] * left_pixles * new_img.shape[0])\
                                       .reshape((new_img.shape[0], left_pixles, 3)), new_img))
            new_img = np.column_stack((new_img, np.array([color_list] * right_pixles * new_img.shape[0])\
                         .reshape((new_img.shape[0], right_pixles, 3))))
        else:
            left_pixles = int((img_width - width) // 2)
            right_pixles = int(img_width - width - left_pixles)
            new_img = new_img[:, left_pixles: -right_pixles, :]
        return new_img

    @staticmethod
    def transformAllPics(path, height, width, color_list=[255, 255, 255]):
        for folder in os.listdir(path):
            for f in os.listdir(path+'/'+folder):
                if '.jpg' in f:
                    print('Processing '+path+'/'+folder+'/'+f)
                    cv2.imwrite(path+'/'+f.replace('.jpg', 'trans.jpg'),
                                ImgProcess.transform(cv2.imread(path+'/'+folder+'/'+f),\
                                          height, width, color_list))
